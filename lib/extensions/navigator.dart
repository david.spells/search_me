import 'package:flutter/material.dart';

extension PushRouteWithName on Navigator {
  static Future<T?> push<T extends Object?>(
    BuildContext context, {
    required WidgetBuilder builder,
    required String name,
  }) {
    Route<T> route = MaterialPageRoute<T>(
      builder: builder,
      settings: RouteSettings(name: name),
    );
    return Navigator.push<T>(context, route);
  }
}

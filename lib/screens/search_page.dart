import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:search_me/extensions/navigator.dart';
import 'package:search_me/future_wrappers/future_wrapper.dart';
import 'package:search_me/future_wrappers/search_wapper.dart';
import 'package:search_me/helpers/localization.dart';
import 'package:search_me/helpers/logger.dart';
import 'package:search_me/screens/web_browser.dart';
import 'package:search_me/widgets/edit_text_getter_setter.dart';

const kAnimationDuration = Duration(milliseconds: 300);

class SearchPage extends StatefulWidget {
  static const String route = 'search_page';

  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  int _selectedIndex = 0;
  final _pageController = PageController();
  // final _mockBingSearch = MockBingSearchWrapper();
  final _googleSearch = GoogleSearchWrapper();
  final _bingSearch = BingSearchWrapper();
  late final List<ISearchWrapper> _searchWrappers = [
    // _mockBingSearch,
    _bingSearch,
    _googleSearch,
  ];
  late final FutureWrapperOneOfMany wrapper = FutureWrapperOneOfMany([
    // _mockBingSearch,
    _bingSearch,
    _googleSearch,
  ]);

  void _setIndex(int index) {
    _selectedIndex = index;
    wrapper.index = index;
    searchText.update();
  }

  void _onTabTapped(int index) {
    setState(() {
      _setIndex(index);
      _pageController.animateToPage(
        index,
        duration: kAnimationDuration,
        curve: Curves.decelerate,
      );
    });
  }

  void _onPageChanged(int index) {
    setState(() => _setIndex(index));
  }

  FutureOr<FutureWrapperOneOfMany> _handleErr(err) {
    String errStr = err.toString();
    logger.d('search API failed $errStr');
    // Dialogs.err(context: context, header: Strings.lang.kConnectionsError, err: err);
    return Future.error(errStr);
  }

  void _refresh() {
    setState(() {
      wrapper.updatedFuture.catchError(_handleErr);
    });
  }

  @override
  void initState() {
    super.initState();
    wrapper.updatedFuture.catchError(_handleErr);
  }

  @override
  void dispose() {
    super.dispose();
    searchText.dispose();
  }

  late EditTextGetterSetter searchText = EditTextGetterSetter(
    getter: () => _searchWrappers[_selectedIndex].searchText,
    setter: (value) => _searchWrappers[_selectedIndex].searchText = value,
  );

  Widget _searchResults(BuildContext context, ISearchWrapper wrapper) {
    ThemeData themeData = Theme.of(context);
    TextTheme textTheme = themeData.textTheme;

    List<ISearchedItem> items = wrapper.items;
    String emptyResults = '';
    if (wrapper.searchText.isEmpty) {
      emptyResults = context.string.emptySearchText;
    } else if (items.isEmpty) {
      emptyResults = context.string.noSearchResults;
    }
    if (emptyResults.isNotEmpty) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30, 0, 48, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(emptyResults, textAlign: TextAlign.center, style: textTheme.headline6),
              const SizedBox(height: 16),
              // This is superfluous but if they click it it works.
              // It's mostly here to guid the user on what the search icon looks like.
              IconButton(
                icon: Icon(Icons.search, size: 48, color: themeData.primaryColor),
                onPressed: () => _refresh(),
              ),
            ],
          ),
        ),
      );
    }
    return ListView.separated(
      padding: EdgeInsets.zero,
      itemCount: items.length,
      itemBuilder: (BuildContext context, int index) {
        ISearchedItem item = items[index];
        return ListTile(
          onTap: () => PushRouteWithName.push(
            context,
            builder: (context) => InAppBrowserScreen(item.link),
            name: 'InAppWebBrowser',
          ).then((value) => _refresh()),
          contentPadding: const EdgeInsets.fromLTRB(4, 0, 0, 0),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.link, style: textTheme.titleMedium),
              const SizedBox(height: 4),
              Text(item.title, style: textTheme.titleLarge),
              const SizedBox(height: 8),
            ],
          ),
          subtitle: Text(item.snippet, style: textTheme.titleMedium),
        );
      },
      separatorBuilder: (BuildContext context, int index) {
        return const SizedBox(height: 16);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    TextTheme textTheme = themeData.textTheme;

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(context.string.searchMe),
      ),
      bottomNavigationBar: SizedBox(
        height: 70,
        child: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: (int index) => _onTabTapped(index),
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: const FaIcon(FontAwesomeIcons.microsoft),
              label: context.string.bing,
            ),
            BottomNavigationBarItem(
              icon: const FaIcon(FontAwesomeIcons.google),
              label: context.string.google,
            ),
          ],
        ),
      ),
      body: FutureBuilder(
        future: wrapper.future,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Padding(
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ...searchText.createTextField(
                  hint: context.string.searchText,
                  textTheme: textTheme,
                  button: SizedBox(
                    height: 50,
                    child: ElevatedButton(
                      onPressed: () => _refresh(),
                      child: const Icon(Icons.search, size: 30),
                    ),
                  ),
                  onFieldSubmitted: (value) => _refresh(),
                ),
                const SizedBox(height: 16),
                Expanded(
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (index) => _onPageChanged(index),
                    children: _searchWrappers.map((wrapper) => _searchResults(context, wrapper)).toList(),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

import 'package:flutter/widgets.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// TODO: Handle initialization of AppLocations for dart only applications
class AppLocalization {
  static AppLocalizations? _current;
  static AppLocalizations get current {
    assert(null != _current);
    return _current!;
  }
}

extension StringHandler on BuildContext {
  AppLocalizations get string {
    AppLocalization._current = AppLocalizations.of(this)!;
    assert(null != AppLocalization._current);
    return AppLocalization._current!;
  }
}

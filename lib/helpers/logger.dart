import 'package:flutter/foundation.dart';
import 'package:flutter_fimber/flutter_fimber.dart';

Logger logger = kReleaseMode ? ReleaseLogger() : DebugLogger();
// Logger logger = DebugLogger();

abstract class Logger {
  void init();
  void v(String message, {dynamic ex, StackTrace? stacktrace});
  void d(String message, {dynamic ex, StackTrace? stacktrace});
  void i(String message, {dynamic ex, StackTrace? stacktrace});
  void w(String message, {dynamic ex, StackTrace? stacktrace});
  void e(String message, {dynamic ex, StackTrace? stacktrace});
}

class DebugLogger extends Logger {
  @override
  void init() {
    Fimber.plantTree(FimberTree(useColors: true));
  }

  void myLog(String level, String message,
      {dynamic ex, StackTrace? stacktrace}) {
    String timeStamp = DateTime.now().toString();
    String tag = LogTree.getTag(stackIndex: 3);
    Fimber.log(level, '$timeStamp $message',
        tag: tag, ex: ex, stacktrace: stacktrace);
  }

  /// Logs VERBOSE level [message]
  /// with optional exception and stacktrace
  @override
  void v(String message, {dynamic ex, StackTrace? stacktrace}) =>
      myLog('V', message, ex: ex, stacktrace: stacktrace);

  /// Logs DEBUG level [message]
  /// with optional exception and stacktrace
  @override
  void d(String message, {dynamic ex, StackTrace? stacktrace}) =>
      myLog('D', message, ex: ex, stacktrace: stacktrace);

  /// Logs INFO level [message]
  /// with optional exception and stacktrace
  @override
  void i(String message, {dynamic ex, StackTrace? stacktrace}) =>
      myLog('I', message, ex: ex, stacktrace: stacktrace);

  /// Logs WARNING level [message]
  /// with optional exception and stacktrace
  @override
  void w(String message, {dynamic ex, StackTrace? stacktrace}) =>
      myLog('W', message, ex: ex, stacktrace: stacktrace);

  /// Logs ERROR level [message]
  /// with optional exception and stacktrace
  @override
  void e(String message, {dynamic ex, StackTrace? stacktrace}) =>
      myLog('E', message, ex: ex, stacktrace: stacktrace);
}

class ReleaseLogger extends Logger {
  @override
  void init() {}
  @override
  void v(String message, {dynamic ex, StackTrace? stacktrace}) {}
  @override
  void d(String message, {dynamic ex, StackTrace? stacktrace}) {}
  @override
  void i(String message, {dynamic ex, StackTrace? stacktrace}) {}
  @override
  void w(String message, {dynamic ex, StackTrace? stacktrace}) {}
  @override
  void e(String message, {dynamic ex, StackTrace? stacktrace}) {}
}

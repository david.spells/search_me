import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:search_me/helpers/localization.dart';
import 'package:search_me/helpers/logger.dart';
import 'package:search_me/models/handmade/json_model.dart';

class NetworkHelperException extends HttpException {
  final int statusCode;

  NetworkHelperException(String message, {required this.statusCode, Uri? uri}) : super(message, uri: uri);

  @override
  String toString() {
    return 'statusCode=$statusCode ${super.toString()}';
  }

  String errorMessage() {
    Map m = json.decode(message);
    return m['error'];
  }
}

typedef HTTPCall = Future<Response> Function();
typedef ExceptionFilter = bool Function(Object error);

class BaseIOClient extends IOClient {
  static HttpClient get httpClient => HttpClient()..maxConnectionsPerHost = 4;

  BaseIOClient() : super(httpClient);
}

class SelfSignedCertIOClient extends IOClient {
  static bool certificateCallback(X509Certificate cert, String host, int port) => true;

  static HttpClient get httpClient => HttpClient()
    ..badCertificateCallback = certificateCallback
    ..maxConnectionsPerHost = 4;

  SelfSignedCertIOClient() : super(httpClient);
}

class NetworkHelper {
  final IOClient ioClient;

  static bool _fullHeaderTest(Object e) {
    if (e is Exception) {
      String message = e.toString();
      return message.contains('closed before full header was received');
    }
    return false;
  }

  static Future<Response> retry({
    required HTTPCall call,
    ExceptionFilter filter = _fullHeaderTest,
    int times = 3,
  }) async {
    for (int iLoop = times; iLoop > 0; iLoop--) {
      try {
        // Putting an await in the call method should not be necessary since we are waiting here for the return value.
        return await call();
      } catch (e) {
        if (!filter(e) || iLoop == 1) rethrow;
        logger.e('Retry HTTP call');
      }
    }
    logger.e('We should never get here');
    return Future.error(AppLocalization.current.internalAPIError);
  }

  NetworkHelper(this.ioClient);
  NetworkHelper.base() : this(BaseIOClient());
  NetworkHelper.selfSigned() : this(SelfSignedCertIOClient());

  dynamic _getJSON(String method, String uri, Response response) {
    logger.d('Response status: ${response.statusCode}, method $method, uri: $uri');
    dynamic jsonData;
    try {
      jsonData = json.decode(response.body);
    } catch (e) {
      if (response.statusCode < 200 || response.statusCode >= 400) {
        String body = response.body;
        logger.e('Error: ${response.statusCode} response body: $body');
        throw NetworkHelperException(body, statusCode: response.statusCode);
      }
      logger.e('Error decoding response body: ${response.body}');
      logger.e('err($e)');
      rethrow;
    }
    const JsonEncoder encoder = JsonEncoder.withIndent('  ');
    String jsonStr = encoder.convert(jsonData);
    logger.d('Response decoded: $jsonStr');
    if (jsonData is Map) {
      Map<String, dynamic> m = jsonData as Map<String, dynamic>;
      String? err = m['error'];
      if (err != null && err.isNotEmpty) throw Exception(err);
    }

    return jsonData;
  }

  Future delete<RT extends JSONModel>({
    required JSONCreator<RT> creator,
    required String uri,
    final Map<String, String> headers = const <String, String>{},
  }) async {
    Response response = await retry(
      call: () => ioClient.delete(Uri.parse(uri), headers: headers),
    );

    return creator(_getJSON('DELETE', uri, response));
  }

  Future get<VT extends JSONModel>({
    required JSONCreator<VT> creator,
    required String uri,
    final Map<String, String> headers = const <String, String>{},
  }) async {
    Response response = await retry(
      call: () => ioClient.get(Uri.parse(uri), headers: headers),
    );

    return creator(_getJSON('GET', uri, response));
  }

  Future patch<RT extends JSONModel>({
    required JSONCreator<RT> creator,
    required JSONModel data,
    required String uri,
    final Map<String, String> headers = const <String, String>{},
  }) async {
    String prettyJSON = data.toString();
    logger.d('method: PATCH url=$uri data=$prettyJSON');
    String body = data.toJSONString();
    Response response = await retry(
      call: () => ioClient.patch(Uri.parse(uri), body: body, headers: headers),
    );

    return creator(_getJSON('PATCH', uri, response));
  }

  Future post<RT extends JSONModel>({
    required JSONCreator<RT> creator,
    required JSONModel data,
    required String uri,
    final Map<String, String> headers = const <String, String>{},
  }) async {
    String prettyJSON = data.toString();
    logger.d('method: POST url=$uri data=$prettyJSON');
    String body = data.toJSONString();
    Response response = await retry(
      call: () => ioClient.post(Uri.parse(uri), body: body, headers: headers),
    );

    return creator(_getJSON('POST', uri, response));
  }

  Future put<RT extends JSONModel>({
    required JSONCreator<RT> creator,
    required JSONModel data,
    required String uri,
    final Map<String, String> headers = const <String, String>{},
  }) async {
    String prettyJSON = data.toString();
    logger.d('method: PUT url=$uri data=$prettyJSON');
    String body = data.toJSONString();
    Response response = await retry(
      call: () => ioClient.put(Uri.parse(uri), body: body, headers: headers),
    );

    return creator(_getJSON('PUT', uri, response));
  }
}

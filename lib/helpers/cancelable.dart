import 'dart:async';

import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:search_me/helpers/logger.dart';

abstract class ICancelable {
  void cancel();
}

class CancelableFunction implements ICancelable {
  VoidCallback cbk;

  CancelableFunction(this.cbk);

  @override
  void cancel() => cbk();
}

class ICancelableOperation implements ICancelable {
  CancelableOperation? cancelableOperation;

  ICancelableOperation(this.cancelableOperation);

  @override
  void cancel() => cancelableOperation?.cancel();
}

class CancelableFuture implements ICancelable {
  final void Function(VoidCallback cbk) setState;
  CancelableOperation? cancelableOperation;
  bool inAsync = false;

  CancelableFuture({required this.setState});

  @override
  void cancel() {
    logger.d('enter');
    cancelableOperation?.cancel();
  }

  Future execute(Future future) async {
    setState(() {
      cancelableOperation = CancelableOperation.fromFuture(future, onCancel: () {});
      inAsync = true;
    });
    try {
      return await future;
    } finally {
      setState(() {
        cancelableOperation = null;
        inAsync = false;
      });
    }
  }
}

class CancelableGroup implements ICancelable {
  final List<ICancelable> items;

  CancelableGroup(this.items);

  @override
  void cancel() {
    logger.d('enter');
    for (ICancelable cancelable in items) {
      try {
        cancelable.cancel();
      } catch (e, stacktrace) {
        logger.e(e.toString());
        logger.e(stacktrace.toString());
      }
    }
  }
}

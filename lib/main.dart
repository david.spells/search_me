import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:search_me/constants/colors.dart';
import 'package:search_me/constants/text_theme.dart';
import 'package:search_me/helpers/logger.dart';
import 'package:search_me/screens/search_page.dart';
import 'package:search_me/screens/web_browser.dart';
import 'package:search_me/shared_state.dart';

void main() async {
  logger.init();
  // This needs to be called before Firebase.initalizeApp()
  WidgetsFlutterBinding.ensureInitialized();
  await SharedState.instance.init();

  runApp(const SearchMeApp());
}

class SearchMeApp extends StatelessWidget {
  const SearchMeApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SharedState state = SharedState.instance;
    BottomNavigationBarThemeData bottomNavigationBarThemeData = BottomNavigationBarThemeData(
      backgroundColor: SearchMeColors.grey.shade100,
    );
    return MaterialApp(
      title: 'Search Me',
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        bottomNavigationBarTheme: bottomNavigationBarThemeData,
        textTheme: AppTextTheme.main,
      ),
      initialRoute: SearchPage.route,
      routes: {
        SearchPage.route: (context) => const SearchPage(),
        InAppBrowserScreen.route: (context) => const InAppBrowserScreen('https://pub.dev'),
      },
      navigatorKey: state.navigatorKey,
      navigatorObservers: [state],
    );
  }
}

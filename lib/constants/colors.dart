import 'package:flutter/material.dart';

extension ColorValues on int {
  int get alpha => (this >> 24) & 0xff;
  int get red => (this >> 16) & 0xff;
  int get green => (this >> 8) & 0xff;
  int get blue => this & 0xff;

  static int valueFrom({required int a, required int r, required int g, required int b}) =>
      a << 24 | r << 16 | g << 8 | b;

  int fraction(int per1000) => 255 - (per1000 * (255 - this) / 1000).round();
  int shade(int per1000) =>
      valueFrom(a: alpha, r: red.fraction(per1000), g: green.fraction(per1000), b: blue.fraction(per1000));
}

class SearchMeColors {
  static const int _greyValue = 0xFF000000;
  static Color _greyTint(int per1000) {
    return Color(_greyValue.shade(per1000));
  }

  static const Color _primaryColor = Colors.black;
  static final int _primaryValue = Colors.black.value;
  static final MaterialColor grey = MaterialColor(
    _primaryValue,
    <int, Color>{
      25: _greyTint(25),
      50: _greyTint(50),
      100: _greyTint(100),
      200: _greyTint(200),
      300: _greyTint(300),
      400: _greyTint(400),
      500: _greyTint(500),
      600: _greyTint(600),
      700: _greyTint(700),
      750: _greyTint(750),
      800: _greyTint(800),
      900: _greyTint(900),
      1000: _primaryColor,
    },
  );
}

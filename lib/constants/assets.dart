import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_svg/flutter_svg.dart';

class Asset {
  final String location;
  final String label;

  const Asset({required this.location, required this.label});

  SvgPicture svg({double height = 24, double? width}) => SvgPicture.asset(
        location,
        semanticsLabel: label,
        height: height,
        width: width,
      );
  SvgPicture coloredSvg({
    double height = 24,
    required Color color,
  }) =>
      SvgPicture.asset(
        location,
        semanticsLabel: label,
        color: color,
        height: height,
      );
  Image get image => Image.asset(location);
  Widget get imageFullScreen {
    return Center(
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(location),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  Future<String> get data async {
    return await rootBundle.loadString(location);
  }
}

class SVGAssets {
  static const duck = Asset(
    location: 'svg/duck.svg',
    label: 'Simplified duck for DuckDuckGo',
  );
}

class FileAssets {
  static const bing = Asset(
    location: 'file/bing.json',
    label: 'Bing search results in a json file for a Mock API call',
  );
}

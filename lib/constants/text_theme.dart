import 'package:flutter/material.dart';
import 'package:search_me/constants/colors.dart';

extension AppTextStyleVariants on TextStyle {
  TextStyle get italic => copyWith(fontStyle: FontStyle.italic);
  TextStyle get regular => copyWith(fontWeight: FontWeight.normal);
  TextStyle get semiBold => copyWith(fontWeight: FontWeight.w600);
  TextStyle get bold => copyWith(fontWeight: FontWeight.bold);
  TextStyle get extraBold => copyWith(fontWeight: FontWeight.w800);
  TextStyle colorize(Color color) => copyWith(color: color);
}

class AppTextTheme {
  static final TextTheme main = TextTheme(
    titleMedium: TextStyle(
      fontSize: 14.0,
      fontWeight: FontWeight.normal,
      color: SearchMeColors.grey.shade400,
    ),
    titleLarge: TextStyle(
      fontSize: 20.0,
      fontWeight: FontWeight.w600,
      color: Colors.purple.shade300,
    ),
    titleSmall: TextStyle(
      fontSize: 10.0,
      fontWeight: FontWeight.normal,
      color: Colors.grey.shade700,
    ),
  );
}

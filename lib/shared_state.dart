import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/widgets.dart';
import 'package:search_me/firebase_options.dart';
import 'package:search_me/helpers/logger.dart';
import 'package:search_me/helpers/network_helper.dart';

class SharedState with NavigatorObserver {
  static final SharedState instance = SharedState();

  // NavigatorObserver code
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  List<Route<dynamic>> routeStack = [];

  Route<dynamic>? get previousRoute => routeStack.length > 1 ? routeStack[routeStack.length - 2] : null;
  Route<dynamic>? get currentRoute => routeStack.isNotEmpty ? routeStack.last : null;

  ValueNotifier<String> routeName = ValueNotifier('');
  void _setRouteName(Route<dynamic> route) {
    routeName.value = route.settings.name ?? routeName.value;
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    logger.d(route.settings.name ?? 'Null route name');
    _setRouteName(route);
    routeStack.add(route);
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    logger.d(previousRoute?.settings.name ?? 'Null route name');
    if (previousRoute != null) _setRouteName(previousRoute);
    routeStack.removeLast();
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {
    logger.d(route.settings.name ?? 'Null route name');
    _setRouteName(route);
    routeStack.removeLast();
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    logger.d(newRoute?.settings.name ?? 'Null route name');
    if (null != newRoute) _setRouteName(newRoute);
    if (null != newRoute) {
      routeStack.removeLast();
      routeStack.add(newRoute);
    }
  }

  // App wide networkHelper
  NetworkHelper networkHelper = NetworkHelper.base();

  // Firebase miscellaneous initialization code
  late final FirebaseRemoteConfig _remoteConfig = FirebaseRemoteConfig.instance;
  String get customSearchAPIKey => _remoteConfig.getString('custom_search_api_key');
  String get rapidAPILKey => _remoteConfig.getString('rapid_api_key');

  Future init() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    await _remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(seconds: 1), // a fetch will wait up to 10 seconds before timing out
      minimumFetchInterval: const Duration(seconds: 10), // fetch parameters will be cached for a maximum of 1 hour
    ));

    await _remoteConfig.fetchAndActivate();
  }
}

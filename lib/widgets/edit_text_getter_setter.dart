import 'package:flutter/material.dart';
import 'package:search_me/constants/colors.dart';

class AppDecoration {
  static InputDecoration input({
    required String hintText,
    String? labelText,
    Widget? suffixIcon,
    Color? hintTextColor,
    Color? enabledBorderColor,
    Color? focusedBorderColor,
    double inset = 16.0,
  }) {
    return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(color: hintTextColor ?? Colors.grey),
      labelText: labelText,
      contentPadding: EdgeInsets.symmetric(vertical: inset, horizontal: inset),
      border: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: enabledBorderColor ?? Colors.blueAccent, width: 2.0),
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: focusedBorderColor ?? Colors.blue, width: 2.0),
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
      ),
      suffixIcon: suffixIcon,
    );
  }
}

class TextFieldState {
  String value = '';
  bool isObscured = false;

  TextFieldState get obscured {
    isObscured = true;
    return this;
  }
}

class EditTextGetterSetter {
  ValueGetter<String> getter;
  ValueSetter<String> setter;
  TextEditingController controller;
  FocusNode focusNode = FocusNode();

  String get value => getter();
  set value(String v) {
    controller.text = v;
    setter(v);
  }

  EditTextGetterSetter({required this.getter, required this.setter})
      : controller = TextEditingController()..text = getter() {
    controller.addListener(() => setter(controller.text));
  }

  void dispose() {
    controller.dispose();
    focusNode.dispose();
  }

  void update() => value = getter();

  List<Widget> createTextField({
    bool autofocus = false,
    String? hint,
    FocusNode? nextFocusNode,
    String label = '',
    required TextTheme textTheme,
    TextInputAction? textInputAction,
    TextInputType? keyboardType,
    FormFieldValidator<String>? validator,
    VoidCallback? onEditingComplete,
    ValueChanged<String>? onChanged,
    ValueChanged<String>? onFieldSubmitted,
    Widget? suffixIcon,
    bool obscureText = false,
    int? maxLines = 1,
    bool hasClearButton = true,
    bool readOnly = false,
    Widget? button,
  }) {
    String hintText = hint ?? label;
    if (nextFocusNode != null) {
      textInputAction = TextInputAction.next;
    }
    onEditingComplete ??= () => nextFocusNode == null ? focusNode.unfocus() : nextFocusNode.requestFocus();
    if (hasClearButton && null == suffixIcon) {
      suffixIcon = IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          value = '';
          onChanged?.call(value);
        },
      );
    }
    return [
      if (label.isNotEmpty)
        Padding(
          padding: const EdgeInsets.only(top: 8, left: 16, right: 16),
          child: Text(label.toUpperCase(), style: textTheme.overline, textAlign: TextAlign.start),
        ),
      Row(
        children: [
          Expanded(
            child: TextFormField(
              decoration: AppDecoration.input(
                hintText: hintText,
                // labelText: label,
                suffixIcon: suffixIcon,
                enabledBorderColor: readOnly ? SearchMeColors.grey.shade100 : null,
                focusedBorderColor: readOnly ? SearchMeColors.grey.shade100 : null,
              ),
              textInputAction: textInputAction,
              focusNode: focusNode,
              controller: controller,
              autofocus: autofocus,
              autocorrect: false,
              textCapitalization: TextCapitalization.none,
              enableSuggestions: false,
              keyboardType: keyboardType,
              validator: validator,
              onEditingComplete: onEditingComplete,
              onFieldSubmitted: onFieldSubmitted,
              onChanged: onChanged,
              maxLines: maxLines,
              obscureText: obscureText,
              readOnly: readOnly,
              style: readOnly ? TextStyle(color: SearchMeColors.grey.shade300) : null,
            ),
          ),
          if (null != button) const SizedBox(width: 8),
          if (null != button) button,
        ],
      ),
    ];
  }
}

import 'package:flutter/material.dart';
import 'package:search_me/screens/search_page.dart';
import 'package:search_me/shared_state.dart';

class AppBarWidgets {
  static double elevation(ThemeData themeData) => themeData.appBarTheme.elevation ?? 4;
  static double? titleSpacing(ThemeData themeData) {
    Route<dynamic>? route = SharedState.instance.currentRoute;
    if (route == null || route.settings.name == SearchPage.route) {
      return null;
    }
    return 0;
  }

  static Widget? backButton(BuildContext context, {VoidCallback? onPressed}) {
    Route<dynamic>? route = SharedState.instance.currentRoute;
    if (route == null || route.settings.name == SearchPage.route) {
      return null;
    }
    onPressed ??= () {
      Navigator.pop(context);
    };
    return IconButton(
      icon: const Icon(Icons.arrow_back_ios_new),
      onPressed: onPressed,
    );
  }
}

import 'dart:async';

import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:search_me/helpers/cancelable.dart';
import 'package:search_me/helpers/logger.dart';

abstract class FutureWrapper<T> extends ChangeNotifier {
  bool _initialized = false;
  late Future<FutureWrapper<T>> _future = Future.value(this);
  Future<FutureWrapper<T>> get future {
    // This is useful when debugging sometimes
    if (kDebugMode && !_initialized) {
      _initialized = true;
      logger.w('Only hot reload should every cause us to get here');
      _future = updatedFuture;
    }
    return _future;
  }

  bool get ready => !refreshing;
  bool inBackground = false;
  bool refreshing = false;
  CancelableOperation? cancelableOperation;
  ICancelable? cancelable;

  FutureWrapper();

  Future<void> onRefresh() async {}

  void clear() {}
  Future<FutureWrapper<T>> get refresh async {
    try {
      refreshing = true;
      await onRefresh();
      postProcess();
      notifyListeners();
    } finally {
      refreshing = false;
      cancelableOperation?.value.whenComplete(() {
        logger.d('CancelableOperation complete');
        cancelableOperation = null;
        cancelable = null;
      });
    }
    return this;
  }

  FutureOr<void> _onCancel() {
    logger.e('CancelableOperation _onCancel');
  }

  Future<FutureWrapper<T>> get updatedFuture {
    logger.d('enter');
    if (refreshing) {
      logger.d('Already refreshing');
    } else {
      clear();
      _future = refresh;
    }
    cancelableOperation = CancelableOperation.fromFuture(future, onCancel: _onCancel);
    cancelable = ICancelableOperation(cancelableOperation);
    return future;
  }

  void postProcess() {
    logger.d('enter');
  }
}

class FutureWrapperList extends FutureWrapper<FutureWrapperList> {
  final List<FutureWrapper> wrappers;
  final ChangeNotifier bgNotifier = ChangeNotifier();

  FutureWrapperList(this.wrappers) : super();

  @override
  void clear() {
    for (FutureWrapper holder in wrappers) {
      holder.clear();
    }
  }

  @override
  Future<FutureWrapper<FutureWrapperList>> get refresh async {
    try {
      await super.refresh;
      List<Future> background = wrappers.where((holder) => holder.inBackground).map((e) => e.refresh).toList();
      await Future.wait(background);
      postBackGroundProcess();
      bgNotifier.notifyListeners();
    } catch (e) {
      String errStr = e.toString();
      logger.e(errStr);
    } finally {
      refreshing = false;
    }
    return this;
  }

  @override
  Future<void> onRefresh() async {
    logger.d('enter num of holders=${wrappers.length}');
    var foreground = wrappers.where((holder) => !holder.inBackground).map((e) => e.refresh);
    await Future.wait(foreground);
  }

  void postBackGroundProcess() {
    logger.d('enter');
  }
}

class FutureWrapperOneOfMany extends FutureWrapper<FutureWrapperOneOfMany> {
  int _index = 0;
  set index(int value) {
    assert(value >= 0 && value < wrappers.length);
    _index = value;
  }

  final List<FutureWrapper> wrappers;

  FutureWrapperOneOfMany(this.wrappers);

  @override
  Future<void> onRefresh() async {
    logger.d('enter num of holders=${wrappers.length}');
    await wrappers[_index].refresh;
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:googleapis/customsearch/v1.dart';
import 'package:googleapis_auth/auth_io.dart';
import 'package:search_me/constants/assets.dart';
import 'package:search_me/future_wrappers/future_wrapper.dart';
import 'package:search_me/helpers/localization.dart';
import 'package:search_me/helpers/logger.dart';
import 'package:search_me/helpers/network_helper.dart';
import 'package:search_me/models/json/bing.dart';
import 'package:search_me/shared_state.dart';

abstract class ISearchedItem {
  String get title;
  String get link;
  String get snippet;
}

abstract class ISearchWrapper {
  String get searchText;
  set searchText(String value);
  List<ISearchedItem> get items;
}

class GoogleSearchItem implements ISearchedItem {
  Result result;

  GoogleSearchItem(this.result);

  @override
  String get title => result.title ?? AppLocalization.current.noTitle;

  @override
  String get link => result.link ?? '';

  @override
  String get snippet => result.snippet ?? '';
}

class GoogleSearchWrapper extends FutureWrapper<GoogleSearchWrapper> implements ISearchWrapper {
  static const String engineId = "dd29e6157458806c8";
  late final _ioClient = clientViaApiKey(SharedState.instance.customSearchAPIKey);
  late final _searchAPI = CustomSearchApi(_ioClient);
  Search search = Search();

  @override
  String searchText = '';

  @override
  Future<void> onRefresh() async {
    if (searchText.isEmpty) {
      search = Search();
      return;
    }
    search = await _searchAPI.cse.list(
      cx: engineId,
      q: searchText,
    );
    logger.d('search complete');
    logger.d(search.items.toString());
  }

  @override
  List<ISearchedItem> get items => search.items?.map((e) => GoogleSearchItem(e)).toList() ?? <ISearchedItem>[];
}

class BingSearchItem implements ISearchedItem {
  BingwebPagesvalue result;

  BingSearchItem(this.result);

  @override
  String get title => result.name.isEmpty ? AppLocalization.current.noTitle : result.name;

  @override
  String get link => result.url;

  @override
  String get snippet => result.snippet;
}

class BingSearchWrapper extends FutureWrapper<BingSearchWrapper> implements ISearchWrapper {
  late final String rapidAPIKey = SharedState.instance.rapidAPILKey;
  Bing search = Bing.blank();

  @override
  String searchText = '';

  Map<String, String> get _headers => <String, String>{
        'X-RapidAPI-Key': rapidAPIKey,
        'X-RapidAPI-Host': 'bing-web-search1.p.rapidapi.com',
        'X-BingApis-SDK': 'true',
      };

  @override
  Future<void> onRefresh() async {
    if (searchText.isEmpty) {
      search = Bing.blank();
      return;
    }
    NetworkHelper networkHelper = SharedState.instance.networkHelper;
    search = await networkHelper.get(
      creator: Bing.create,
      uri: 'https://bing-web-search1.p.rapidapi.com/search?q=$searchText&safeSearch=Off&textFormat=Raw',
      headers: _headers,
    );
    logger.d('successful');
  }

  @override
  List<ISearchedItem> get items => search.webPages.value.map((e) => BingSearchItem(e)).toList();
}

class MockBingSearchWrapper extends FutureWrapper<BingSearchWrapper> implements ISearchWrapper {
  Bing search = Bing.blank();

  @override
  String searchText = 'dogs';

  @override
  Future<void> onRefresh() async {
    if (searchText.isEmpty) {
      search = Bing.blank();
      return;
    }
    String bingJsonString = await FileAssets.bing.data;
    Map<String, dynamic> bingJson = json.decode(bingJsonString);
    search = Bing(bingJson);
    logger.d('successful\n$bingJsonString');
  }

  @override
  List<ISearchedItem> get items => search.webPages.value.map((e) => BingSearchItem(e)).toList();
}

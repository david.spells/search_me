import 'dart:collection';
import 'dart:convert';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:search_me/helpers/logger.dart';

extension CloneList<T> on List<T> {
  List<T> get copy => map((e) {
        return e;
      }).toList();
}

typedef JSONCreator<T extends JSONModel> = T Function(Map<String, dynamic>);

class JSONModelFactory<T extends JSONModel> {
  static final Map<Type, JSONCreator> type2Creator = <Type, JSONCreator>{};

  JSONModelFactory(JSONCreator<T> creator) {
    type2Creator[T] = creator;
  }

  static T create<T extends JSONModel>(Map<String, dynamic> data) {
    if (!JSONModelFactory.type2Creator.containsKey(T)) {
      String s = T.toString();
      logger.e('Null factory method for $s');
    }
    JSONCreator<T> creator = JSONModelFactory.type2Creator[T]! as JSONCreator<T>;
    return creator(data);
  }
}

abstract class JSONModel implements Comparable {
  final Map<String, dynamic> jsonData;

  bool get isEmpty => jsonData.isEmpty;

  const JSONModel(this.jsonData);
  JSONModel.fromJSON(String jsonStr) : jsonData = json.decode(jsonStr);

  static Map<String, dynamic> caseInsensitiveMap(Map<String, dynamic> jsonData) => LinkedHashMap(
        equals: (a, b) => a.toLowerCase() == b.toLowerCase(),
        hashCode: (key) => key.toLowerCase().hashCode,
      )..addAll(jsonData);

  static _int(dynamic value, int defaultValue) {
    if (value == null) return defaultValue;
    if (value is int) return value;
    if (value is double) return value.round();
    if (value is String) return int.tryParse(value) ?? defaultValue;
  }

  static int getint(Map<String, dynamic> jsonData, String key, {int defaultValue = 0}) =>
      _int(jsonData[key], defaultValue);

  static _double(dynamic value, double defaultValue) {
    if (value == null) return defaultValue;
    if (value is double) return value;
    if (value is int) return value.toDouble();
    if (value is String) return double.tryParse(value) ?? defaultValue;
    return defaultValue;
  }

  static double getdouble(Map<String, dynamic> jsonData, String key, {defaultValue = 0.0}) =>
      _double(jsonData[key], defaultValue);

  static String _string(dynamic value, String defaultValue) {
    if (null == value) return '';
    if (value is String) return value;
    return value.toString();
  }

  static String getString(Map<String, dynamic> jsonData, String key, {String defaultValue = ''}) =>
      _string(jsonData[key], defaultValue);

  static Map<String, dynamic> mapMap(Map value) =>
      value.map((key, value) => MapEntry<String, dynamic>(_string(key, ''), value));

  static Map<String, dynamic> getMap(Map<String, dynamic> jsonData, String key) {
    if (jsonData.containsKey(key)) {
      var value = jsonData[key];
      if (value is Map) return mapMap(value);
    }
    return <String, dynamic>{};
  }

  static Map<String, dynamic> getJSONStrMap(Map<String, dynamic> jsonData, String key) {
    if (jsonData.containsKey(key)) {
      var value = jsonData[key];
      if (value is String) {
        Map<String, dynamic> jsonData = json.decode(value);
        return mapMap(jsonData);
      }
      if (value is Map) return mapMap(value);
    }
    return <String, dynamic>{};
  }

  static List<T> getList<T>(
    Map<String, dynamic> jsonData,
    String key,
    T Function(Object o) creator,
  ) {
    if (jsonData.containsKey(key)) {
      var value = jsonData[key];
      if (value is List) return List<T>.from(value.map((e) => e is T ? e : creator(e)));
    }
    return <T>[];
  }

  static T getType<T>(dynamic o) {
    if (o is T) return o;
    if (T is double) return _double(o, 0.0);
    if (T is int) return _int(o, 0);
    if (T is String) return _string(o, '') as T;
    assert(false);
    return o as T;
  }

  static bool getbool(Map<String, dynamic> jsonData, String key, {bool defaultValue = false}) {
    var value = jsonData[key];
    if (value is bool) return value;
    if (value is int) return value != 0;
    if (value is double) return value != 0.0;
    if (value is String) return value == 'On' || value == 'Yes';

    return defaultValue;
  }

  static dynamic jsonItem(dynamic item) {
    if (item is JSONModel) return item.toJSON();
    if (item is List) return item.map((e) => jsonItem(e)).toList();
    return item;
  }

  Map<String, dynamic> toJSON();

  @override
  String toString() {
    var jsonData = toJSON();
    const JsonEncoder encoder = JsonEncoder.withIndent('  ');
    return encoder.convert(jsonData);
  }

  String toJSONString() {
    var jsonData = toJSON();
    const JsonEncoder encoder = JsonEncoder();
    return encoder.convert(jsonData);
  }

  void copy(o) {}
}

class RawJSONModel extends JSONModel {
  RawJSONModel(Map<String, dynamic> jsonData) : super(jsonData);
  RawJSONModel.blank() : super({});

  @override
  int compareTo(other) {
    return 0;
  }

  @override
  Map<String, dynamic> toJSON() => jsonData;
}

bool modelListEquals<T>(List<T>? a, List<T>? b) => listEquals(a, b);
int modelListCompareTo<T>(List<T>? a, List<T>? b) {
  if (a == null) return b == null ? 0 : -1;
  if (b == null) return 1;
  if (a.length != b.length) return a.length > b.length ? 1 : -1;
  if (identical(a, b)) return 0;
  for (int index = 0; index < a.length; index += 1) {
    final aItem = a[index];
    final bItem = b[index];
    if (aItem == null) return bItem == null ? 0 : -1;
    if (bItem == null) return 1;
    if (aItem.runtimeType != bItem.runtimeType) return aItem.toString().compareTo(bItem.toString());
    // ignore: unused_local_variable
    final Comparable<T> aComparable = aItem as Comparable<T>;
    // ignore: unused_local_variable
    final Comparable<T> bComparable = bItem as Comparable<T>;
    final int ret = aItem.compareTo(bItem);
    if (ret != 0) return ret;
  }
  return 0;
}

int modelHashList(List<Object> l) => hashList(l);

extension BoolCompareTo on bool {
  int compareTo(bool other) {
    if (this == other) return 0;
    return this ? 1 : -1;
  }
}

bool mapEquals<T, U>(Map<T, U>? a, Map<T, U>? b) {
  if (a == null) return b == null;
  if (b == null || a.length != b.length) return false;
  if (identical(a, b)) return true;
  for (final T key in a.keys) {
    if (!b.containsKey(key) || b[key] != a[key]) {
      return false;
    }
  }
  return true;
}

int modelMapCompareTo<K, V>(Map<K, V> a, Map<K, V> b) {
  if (a.keys.length != b.keys.length) return a.length > b.length ? 1 : -1;
  if (!identical(a, b)) {
    for (final K key in a.keys) {
      if (!b.containsKey(key)) return 1;
      final aItem = a[key];
      final bItem = b[key];
      if (aItem == null) return bItem == null ? 0 : -1;
      if (bItem == null) return 1;
      if (aItem.runtimeType != bItem.runtimeType) return aItem.toString().compareTo(bItem.toString());
      // ignore: unused_local_variable
      final Comparable<V> aComparable = aItem as Comparable<V>;
      // ignore: unused_local_variable
      final Comparable<V> bComparable = bItem as Comparable<V>;
      final int ret = aItem.compareTo(bItem);
      if (ret != 0) return ret;
    }
  }
  return 0;
}

#!/bin/bash

SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd "$SCRIPT_PATH" > /dev/null

do_generate() {
  src_folder=$1
  file_name=$2
  output_folder=../lib/models/json
  echo "generating $file_name.dart"
  dart run ../bin/generate_model_from_json.dart "$src_folder"/"$file_name".json > $output_folder/"$file_name".dart
}

do_generate ../file bing

dart format --line-length=120 lib/models

popd > /dev/null
